local class = require("libs/middleclass")
local point = require("point")
local inspect = require("utils/inspect")
local point_list = class("point_list")

local EPSILON = 0.00001

-- Point on line segment collision check
local point_on_line = function(a, b, p)
	return math.abs(p:get_distance(a) + p:get_distance(b) - a:get_distance(b)) < EPSILON
end

-- Point in rectangle collision check
local point_in_rect = function(a, b, p)
	return (a.x <= p.x and b.x >= p.x) and (a.y <= p.y and b.y >= p.y)
end
	
-- Robust line on line intersection check 
local line_on_line = function(a, b, c, d)
    local denominator = (b.x - a.x) * (d.y - c.y) - (b.y - a.y) * (d.x - c.x)
	if denominator == 0 then
		return point_on_line(a, b, c) or point_on_line(a, b, d)
	end
    local numerator1 = (a.y - c.y) * (d.x - c.x) - (a.x - c.x) * (d.y - c.y)
	local numerator2 = (a.y - c.y) * (b.x - a.x) - (a.x - c.x) * (b.y - a.y)
    local r = numerator1 / denominator
    local s = numerator2 / denominator
    return (r >= 0 and r <= 1) and (s >= 0 and s <= 1)
end

function point_list:initialize()
	self.list = {}
end

function point_list:push(p)
	table.insert(self.list, p)
end

function point_list:pop()
	local p = self.list[#self.list]
	table.remove(self.list, #self.list)
	return p
end

function point_list:peek()
	return self.list[#self.list]
end

-- Get point array for drawing
function point_list:get_open_points()
	local points = {}
	for _, v in ipairs(self.list) do
		table.insert(points, v.x)
		table.insert(points, v.y)
	end
	return points
end

-- Get point array for drawing
function point_list:get_closed_points()
	local points = {}
	for _, v in ipairs(self.list) do
		table.insert(points, v.x)
		table.insert(points, v.y)
	end
	table.insert(points, self.list[1].x)
	table.insert(points, self.list[1].y)
	return points
end

-- Get the direction of the top line
function point_list:get_last_vector()	
	--if #self.list < 2 then
	--	return
	--end
	local p1 = self.list[#self.list-1]
	local p2 = self.list[#self.list]
	if p1.x == p2.x then
		if p1.y < p2.y then
			return point(0, 1)
		else
			return point(0, -1)
		end
	elseif p1.y == p2.y then
		if p1.x < p2.x then
			return point(1, 0)
		else
			return point(-1, 0)
		end
	end
end

-- Check if line "l" intersects with the list of lines
-- Return intersection snap point
function point_list:line_on_polyline(l)
	local lines = {}
	for i, p1 in ipairs(self.list) do
		local p2 = self.list[i%#self.list+1]
		if line_on_line(l[1], l[2], p1, p2) then
			table.insert(lines, {p1, p2})
			if #lines == 2 then
				break
			end
		end
	end
	if #lines == 1 then
		if lines[1][1].x == lines[1][2].x then
			return point(lines[1][1].x, l[1].y)
		else
			return point(l[1].x, lines[1][1].y)
		end
	elseif #lines == 2 then
		if lines[1][1] == lines[2][1] or lines[1][1] == lines[2][2] then
			return lines[1][1]:copy()
		elseif lines[1][2] == lines[2][1] or lines[1][2] == lines[2][2] then
			return lines[1][2]:copy()
		end
	end
	return nil
end

-- Check if point pos is on list of lines
-- Return the line if so
function point_list:point_on_polyline(p)
	for i, p1 in ipairs(self.list) do
		local p2 = self.list[i%#self.list+1]
		if point_on_line(p1, p2, p) then
			return {p1, p2}
		end	
	end
	return nil
end

-- Check if point p is in the polygon
function point_list:point_in_polygon(p)
	local min = math.min
	local max = math.max
	local counter = 0
	for i, p1 in ipairs(self.list) do
		local p2 = self.list[i%#self.list+1]
		if (p.y > min(p1.y,p2.y))
			and (p.y <= max(p1.y, p2.y))
			and (p.x <= max(p1.x, p2.x))
			and (p1.y ~= p2.y) then
			local xinters = (p.y - p1.y) * (p2.x - p1.x) / (p2.y - p1.y) + p1.x
			if p1.x == p2.x or p.x <= xinters then
				counter = counter + 1
			end
		end
	end
	if counter % 2 == 0 then
		return false
	end
	return true
end

function point_list:__tostring()
	local str = ""
	for _, v in ipairs(self.list) do
		str = str .. tostring(v)
	end
	if str == "" then
		return "Empty point list"
	end
	return str
end

return point_list

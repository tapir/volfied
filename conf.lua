function love.conf(t)
	t.version = "0.10.0"
	t.window.title = "Volfied"
	t.window.width = 480
	t.window.height = 270
	t.window.vsync = false
end

local class = require("libs/middleclass")
local action_stack = require("action_stack")
local scene = require("scene")
local utils = require("utils")
local point = require("point")
local point_list = require("point_list")
local inspect = require("utils/inspect")

local player = class("player")

-- Player state enums
player.static.EDGE_MODE = 0
player.static.TRACE_MODE = 1

-- Speed vector enums
player.static.VECTOR_UP = point(0, -1)
player.static.VECTOR_DOWN = point(0, 1)
player.static.VECTOR_LEFT = point(-1, 0)
player.static.VECTOR_RIGHT = point(1, 0)
player.static.VECTOR_ZERO = point(0, 0)

-- Scalar speed
player.static.SPEED = 68

function player:initialize(x, y, up, down, left, right, shield)
	-- Direction and pos
	self.vector = point(0, -1)
	self.pos = point(x, y)
	self.snap_pos = self.pos:get_snap()
	
	-- Collision shapes
	self.line = {
		point(x, y-0.1),
		point(x, y-GRID_SIZE),
	}
	self.box = {
		point(x-GRID_SIZE, y-GRID_SIZE),
		point(x+GRID_SIZE, y+GRID_SIZE),
	}
	
	-- Control keys
	self.up_binding = up
	self.down_binding = down
	self.left_binding = left
	self.right_binding = right
	self.shield_binding = shield

	-- Control stack
	self.mov_actions = action_stack:new()
	self.shield_action = false
	
	-- Qix sprite
	self.image = love.graphics.newImage("assets/ship2.png")
	self.ox = self.image:getWidth() / 2
	self.oy = self.image:getHeight() / 2
	self.orientation = 0
	
	-- Current state
	self.state = player.EDGE_MODE
	
	-- Actions
	self.pact = action_stack.NO_ACTION
	self.cact = action_stack.NO_ACTION
	
	-- Trace line
	self.trace_line = point_list()
end

function player:reset_line()
	if self.vector == player.VECTOR_UP then
		self.line = {
			point(self.pos.x, self.pos.y-0.1),
			point(self.pos.x, self.pos.y-GRID_SIZE),
		}
	elseif self.vector == player.VECTOR_DOWN then
		self.line = {
			point(self.pos.x, self.pos.y+0.1),
			point(self.pos.x, self.pos.y+GRID_SIZE),
		}	
	elseif self.vector == player.VECTOR_LEFT then
		self.line = {
			point(self.pos.x-0.1, self.pos.y),
			point(self.pos.x-GRID_SIZE, self.pos.y),
		}
	elseif self.vector == player.VECTOR_RIGHT then
		self.line = {
			point(self.pos.x+0.1, self.pos.y),
			point(self.pos.x+GRID_SIZE, self.pos.y),
		}
	end
end

function player:reset_box()
	self.box = {
		point(self.pos.x-GRID_SIZE, self.pos.y-GRID_SIZE),
		point(self.pos.x+GRID_SIZE, self.pos.y+GRID_SIZE),
	}
end

function player:set_pos(p)
	self.pos:set_to_point(p)
	self.snap_pos = self.pos:get_snap()
	
	-- Update collision shapes
	self:reset_line()
	self:reset_box()
end

function player:draw()
	if self.state == player.TRACE_MODE then
		-- Draw trace line
		local points = self.trace_line:get_open_points()

		if self.trace_line:peek() ~= self.pos then
			table.insert(points, self.pos.x)
			table.insert(points, self.pos.y)
		end

		if #points >= 4 then
			love.graphics.setColor(0,0,255)
			love.graphics.line(points)
		end
	else
		-- Draw shield
		love.graphics.setColor(255, 255, 0)
		love.graphics.circle("line", self.pos.x, self.pos.y, 12, 10)
	end

	-- Draw qix
	love.graphics.setColor(255, 255, 255)
	love.graphics.draw(self.image, self.pos.x, self.pos.y, self.orientation, 1, 1, self.ox, self.oy)	
end

function player:update(dt)
	if self.state == player.TRACE_MODE then
		self:trace_mode(dt)
	elseif self.state == player.EDGE_MODE then
		self:edge_mode(dt)
	end
end

function player:edge_mode(dt)
	self.cact = self.mov_actions:get_action()
	
	-- Get movement vector
	if self.cact == action_stack.UP_ACTION then
		self.vector:set_to_point(player.VECTOR_UP)
		self.orientation = 0
	elseif self.cact == action_stack.RIGHT_ACTION then
		self.vector:set_to_point(player.VECTOR_RIGHT)
		self.orientation = math.pi / 2
	elseif self.cact == action_stack.DOWN_ACTION then
		self.vector:set_to_point(player.VECTOR_DOWN)
		self.orientation = math.pi
	elseif self.cact == action_stack.LEFT_ACTION then
		self.vector:set_to_point(player.VECTOR_LEFT)
		self.orientation = -math.pi / 2
	elseif self.cact == action_stack.NO_ACTION then
		self.vector:set_to_point(player.VECTOR_ZERO)
		self:set_pos(self.snap_pos)
		return
	end
	
	-- Update player position
	local p = point (
		self.pos.x + dt * self.vector.x * player.SPEED,
		self.pos.y + dt * self.vector.y * player.SPEED
	)
	
	local line = s.edge_line:point_on_polyline(p)
	if line ~= nil then
		-- If vertical
		if line[1].x == line[2].x then
			p.x = line[1].x
		else
			p.y = line[1].y
		end
		self:set_pos(p)
	else
		if self.shield_action and s.edge_line:point_in_polygon(p) then
			-- TODO: Snap to correct position
			self:switch_state(player.TRACE_MODE)
			self:set_pos(p:get_snap())
			return
		else
			if self.vector == player.VECTOR_UP then
				if p.y < self.snap_pos.y then
					self:set_pos(point(self.pos.x, self.snap_pos.y))
				end
			elseif self.vector == player.VECTOR_DOWN then
				if p.y > self.snap_pos.y then
					self:set_pos(point(self.pos.x, self.snap_pos.y))
				end		
			elseif self.vector == player.VECTOR_LEFT then
				if p.x < self.snap_pos.x then
					self:set_pos(point(self.snap_pos.x, self.pos.y))
				end
			elseif self.vector == player.VECTOR_RIGHT then
				if p.x > self.snap_pos.x then
					self:set_pos(point(self.snap_pos.x, self.pos.y))
				end
			end
		end
	end
end

function player:trace_mode(dt)
	-- Previous action can't be NO_ACTION
	if self.cact ~= 0 then self.pact = self.cact end
	self.cact = self.mov_actions:get_action()
	
	local turning = math.abs(self.pact * self.cact) == 2
	
	-- Collects trace line
	if turning then
		-- Snap t ogrid every turn
		self:set_pos(self.snap_pos)
		
		-- Add a successfull line at corner point
		if self.trace_line:peek() ~= self.pos then
			self.trace_line:push(self.pos:copy())
		else
			-- Delete point if last trace is extended or undone
			local v = self.trace_line:get_last_vector()
			if self.vector.x == self.vector.x or self.vector.y == self.vector.y then
				self.trace_line:pop()
			end	
		end
	end
	
	-- Get movement vector
	if self.cact == action_stack.UP_ACTION then
		self.vector:set_to_point(player.VECTOR_UP)
		self.orientation = 0
	elseif self.cact == action_stack.RIGHT_ACTION then
		self.vector:set_to_point(player.VECTOR_RIGHT)
		self.orientation = math.pi / 2
	elseif self.cact == action_stack.DOWN_ACTION then
		self.vector:set_to_point(player.VECTOR_DOWN)
		self.orientation = math.pi
	elseif self.cact == action_stack.LEFT_ACTION then
		self.vector:set_to_point(player.VECTOR_LEFT)
		self.orientation = -math.pi / 2
	elseif self.cact == action_stack.NO_ACTION then
		self.vector:set_to_point(player.VECTOR_ZERO)
		self:set_pos(self.snap_pos)
		return
	end
	
	self:reset_line()
	
	-- Update position
	local p = point(
		self.pos.x + dt * self.vector.x * player.SPEED,
		self.pos.y + dt * self.vector.y * player.SPEED
	)
	
	-- Check if on edge
	local s = s.edge_line:line_on_polyline(self.line)
	if s ~= nil then
		if not self.shield_action then
			self:set_pos(s)
			self:switch_state(player.EDGE_MODE)
			return
		end
	else
		self.shield_action = false
	end
	
	-- Else update the player position
	self:set_pos(p)
end

function player:switch_state(mode)
	if mode == player.EDGE_MODE then
		self.shield_action = false
	elseif mode == player.TRACE_MODE then
		self.trace_line.list = {}
		self.trace_line:push(self.snap_pos:copy())
	end
	self.state = mode
end

function player:keypressed(key)
	if key == self.up_binding then
		self.mov_actions:add(action_stack.UP_ACTION)
	elseif key == self.down_binding then
		self.mov_actions:add(action_stack.DOWN_ACTION)
	elseif key == self.left_binding then
		self.mov_actions:add(action_stack.LEFT_ACTION)
	elseif key == self.right_binding then
		self.mov_actions:add(action_stack.RIGHT_ACTION)
	end
	
	if key == self.shield_binding then
		self.shield_action = true
	end
end

function player:keyreleased(key)
	if key == self.up_binding then
		self.mov_actions:remove(action_stack.UP_ACTION)
	elseif key == self.down_binding then
		self.mov_actions:remove(action_stack.DOWN_ACTION)
	elseif key == self.left_binding then
		self.mov_actions:remove(action_stack.LEFT_ACTION)
	elseif key == self.right_binding then
		self.mov_actions:remove(action_stack.RIGHT_ACTION)
	end
	
	if key == self.shield_binding then
		self.shield_action = false
	end		
end

return player

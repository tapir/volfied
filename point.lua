local class = require("libs/middleclass")
local utils = require("utils")

local point = class("point")

function point:initialize(x, y)
	self.x = x
	self.y = y
end

function point:copy()
	return point:new(self.x, self.y)
end

function point:get_distance(p)
	return math.sqrt((p.x - self.x)^2 + (p.y - self.y)^2)
end

function point:set(x, y)
	self.x = x
	self.y = y
end

function point:set_to_point(p)
	self.x = p.x
	self.y = p.y
end

function point:get_snap()
	local x = utils.round_to_factor(self.x, GRID_SIZE)
	local y = utils.round_to_factor(self.y, GRID_SIZE)
	return point(x, y)
end

function point:__eq(p) 
	return self.x == p.x and self.y == p.y
end

function point:__tostring()
	return "[" .. tostring(utils.round(self.x,2)) .. " " .. tostring(utils.round(self.y,2)) .. "]"
end

return point

local utils = {}

function utils.round(num, d)
	local mult = 10^(d or 0)
	return math.floor(num * mult + 0.5) / mult
end

function utils.round_to_factor(num, fact)
	local r = num % fact
	
	if r >= fact / 2 then
		return num + fact - r
	else
		return num - r
	end
end

return utils

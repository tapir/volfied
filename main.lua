local scene = require("scene")
local player = require("player")

GRID_SIZE = 2

debug = true

-- Display mode settings
 base_width = 480
local base_height = 270
local scale = 3
local fullscreen = false
local display = 2
local vsync = true
p = player:new(16, 12, "up", "down", "left", "right", "space")
s = scene:new()

local reset_display_mode = function()
	love.window.setMode(base_width*scale, base_height*scale, {display=display, fullscreen=fullscreen, vsync=vsync})
end

function love.load()
	reset_display_mode()
	love.graphics.setNewFont("assets/PixelCarnageMono.ttf", 16)
	love.graphics.setDefaultFilter("nearest", "nearest")
	love.graphics.setLineStyle("rough")
	love.graphics.setLineJoin("miter")
end
 
function love.update(dt)
	p:update(dt)
end
 
function love.draw()
	if debug then
		local fscreen_str = " | Fullscreen: " .. tostring(fullscreen)
		local vsync_str = " | Vsync: " .. tostring(vsync)
		local display_str = " | Display: " .. tostring(display)
		love.graphics.setColor(255, 255, 0)
		love.graphics.print("FPS: " .. tostring(love.timer.getFPS()) .. fscreen_str .. vsync_str .. display_str, 2, 2)
	end
	love.graphics.push()
	love.graphics.scale(scale, scale)
	s:draw()
	p:draw()
	love.graphics.pop()
end

function love.keypressed(key)
	if key == "f1" then
		scale = scale - 1
		if scale < 2 then scale = 2 end
		reset_display_mode()
	elseif key == "f2" then
		scale = scale + 1
		if scale > 4 then scale = 4 end
		reset_display_mode()
	elseif key == "f3" then
		fullscreen = not fullscreen
		reset_display_mode()
	elseif key == "f4" then
		vsync = not vsync
		reset_display_mode()
	elseif key == "f5" then
		debug = not debug
	elseif key == "1" then
		display = 1
		reset_display_mode()
	elseif key == "2" then
		display = 2
		reset_display_mode()
	end
	
	p:keypressed(key)
end

function love.keyreleased(key)
	p:keyreleased(key)
end

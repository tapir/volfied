local class = require("libs/middleclass")

local action_stack = class("action_stack")

action_stack.static.NO_ACTION = 0
action_stack.static.UP_ACTION = 1
action_stack.static.DOWN_ACTION = -1
action_stack.static.LEFT_ACTION = -2
action_stack.static.RIGHT_ACTION = 2

function action_stack:initialize()
	self.stack = {}
end

function action_stack:add(value)
	-- Max 4 keys allowed simultaneously
	if #self.stack < 4 then
		table.insert(self.stack, value)
	end
end

function action_stack:remove(value)
	-- Find the index that holds the value
	for i, v in ipairs(self.stack) do
		if v == value then
			table.remove(self.stack, i)
			break
		end
	end
end

function action_stack:get_action()
	local r = self.stack[#self.stack]
	
	if r == nil then
		return action_stack.NO_ACTION
	end
	
	return r
end

function action_stack:__tostring()
	local str = "["
	
	if self.previous_action == action_stack.NO_ACTION then
		str = str .. "NONE"
	elseif self.previous_action == action_stack.UP_ACTION then
		str = str .. "UP"
	elseif self.previous_action == action_stack.DOWN_ACTION then
		str = str .. "DOWN"
	elseif self.previous_action == action_stack.LEFT_ACTION then
		str = str .. "LEFT"
	elseif self.previous_action == action_stack.RIGHT_ACTION then
		str = str .. "RIGHT"
	end
	
	str = str .. "] ["

	local j = 4 - #self.stack
	
	for i=1,j do
		str = str .. "NONE"
		if i ~= 4 then
			str = str .. ", "
		end
	end
	
	for i, v in ipairs(self.stack) do
		if v == action_stack.NO_ACTION then
			str = str .. "NONE"
		elseif v == action_stack.UP_ACTION then
			str = str .. "UP"
		elseif v == action_stack.DOWN_ACTION then
			str = str .. "DOWN"
		elseif v == action_stack.LEFT_ACTION then
			str = str .. "LEFT"
		elseif v == action_stack.RIGHT_ACTION then
			str = str .. "RIGHT"
		end
		
		if i < #self.stack then
			str = str .. ", "
		end
	end
	
	return str .. "]"
end

return action_stack

local class = require("libs/middleclass")
local point_list = require("point_list")
local point = require("point")

local scene = class("scene")

function scene:initialize()
	self.edge_line = point_list:new()
	self.edge_line.list = {
		point(12,12),
		point(68,12),
		point(68,120),
		point(68,122),
		point(70,122),
		point(70,120),
		point(128,120),
		point(128,12),
		point(468,12),
		point(468,200),
		point(96,200),
		point(96,220),
		point(468,220),
		point(468,256),
		point(12,256),
	}
end

function scene:draw()
	local points = self.edge_line:get_closed_points()
	love.graphics.setColor(255, 255, 255)
	love.graphics.line(points)
end

return scene
